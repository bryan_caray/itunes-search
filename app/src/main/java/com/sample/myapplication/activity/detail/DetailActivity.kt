package com.sample.myapplication.activity.detail

import android.os.Build
import android.view.Gravity
import com.sample.myapplication.R
import com.sample.myapplication.data.Results
import com.sample.myapplication.databinding.ActivityDetailsBinding
import com.yondu.basekotlin.global.mvp.MvpActivity

class DetailActivity : MvpActivity<ActivityDetailsBinding, DetailPresenter, DetailView>(), DetailView {

    override val presenter get() = DetailPresenter(this)

    override val layoutId get() = R.layout.activity_details

    override val isInnerActivity get() = true

    override val toolbarBinding get() = binding.toolbarView

    override val toolbarTitleGravity get() = Gravity.CENTER

    override var toolbarTitle = "Movie Details"

    private lateinit var results: Results

    override fun onViewCreated() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.ivMovie.transitionName = "image_transition"
            binding.tvMovieTitle.transitionName = "name_transition"
            binding.tvGenre.transitionName = "genre_transition"
            binding.tvMoviePrice.transitionName = "price_transition"
        }
        results = intent.getSerializableExtra("result") as Results
        binding.results = results


    }
}
