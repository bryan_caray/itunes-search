package com.sample.myapplication.activity.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.GridLayoutManager
import com.sample.myapplication.R
import com.sample.myapplication.activity.detail.DetailActivity
import com.sample.myapplication.adapter.GridItemDecoration
import com.sample.myapplication.adapter.ResultAdapter
import com.sample.myapplication.data.Results
import com.sample.myapplication.databinding.ActivityMainBinding
import com.sample.myapplication.databinding.AdapterResultBinding
import com.yondu.basekotlin.global.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : MvpActivity<ActivityMainBinding, MainPresenter, MainView>(), MainView, ResultAdapter.Listener {
    override fun onDeliveryClick(item: Results, binding: AdapterResultBinding) {
        val bundle = Bundle()
        bundle.putSerializable("result", item)
        val p1 = Pair.create(binding.ivMovie as View, "image_transition")
        val p2 = Pair.create(binding.tvMovieTitle as View, "name_transition")
        val p3 = Pair.create(binding.tvGenre as View, "genre_transition")
        val p4 = Pair.create(binding.tvMoviePrice as View, "price_transition")
        val pairs = ArrayList<Pair<View, String>>()
        pairs.add(p1)
        pairs.add(p2)
        pairs.add(p3)
        pairs.add(p4)
        val pairArray = pairs.toTypedArray()
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, *pairArray)
        openActivityTransition(DetailActivity::class.java, bundle, options)


    }

    override fun onFailedFetch(error: String) {
        binding.swipeContainer.isRefreshing = false
        binding.isNoResultVisible = true
        if (presenter.getRealmResult().isNotEmpty()) {
            binding.isNoResultVisible = false
        }

    }

    override fun onSuccessFetch() {
        binding.swipeContainer.isRefreshing = false
        binding.isNoResultVisible = false
        var count: Int = presenter.getRealmResult().size
        adapter.items = presenter.getRealmResult()
        adapter.notifyDataSetChanged()
    }

    override val presenter get() = MainPresenter(this)

    override val layoutId get() = R.layout.activity_main

    override val toolbarBinding get() = binding.toolbarView
    override var isMainActivity = true
    override val toolbarTitleGravity get() = Gravity.CENTER

    override var toolbarTitle = "Movies"
    override var showBack = false
    private var adapter = ResultAdapter(presenter.getRealmResult())

    override fun onViewCreated() {
        binding.isNoResultVisible = false
        binding.deliveries.layoutManager = GridLayoutManager(this, 2)
        binding.deliveries.addItemDecoration(GridItemDecoration(10, 2))
        presenter.getResult()
        binding.deliveries.adapter = adapter
        adapter.listener = this
        tvDelete.setOnClickListener {
            etSearch.text.clear()
        }
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                adapter.items = presenter.getRealmResult(etSearch.text.toString())
                adapter.notifyDataSetChanged()
                if (etSearch.text.isNotEmpty()) {
                    tvDelete.visibility = View.VISIBLE
                    binding.swipeContainer.isEnabled = false
                } else {
                    binding.swipeContainer.isEnabled = true
                    tvDelete.visibility = View.GONE
                }
            }
        })


    }

    fun fetch() {
        presenter.getResult()
    }
}
