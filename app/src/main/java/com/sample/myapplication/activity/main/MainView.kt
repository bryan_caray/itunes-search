package com.sample.myapplication.activity.main

import com.sample.myapplication.MvpView

interface MainView : MvpView.ActivityFragment {
    abstract fun onFailedFetch(error: String)
    fun onSuccessFetch()
}