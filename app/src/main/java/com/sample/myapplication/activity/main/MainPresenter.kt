package com.sample.myapplication.activity.main

import android.annotation.SuppressLint
import android.util.Log
import com.sample.myapplication.MvpPresenter
import com.sample.myapplication.data.ApiResult
import com.sample.myapplication.data.Results
import io.realm.Case
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainPresenter(view: MainView) : MvpPresenter<MainView>(view) {

    @SuppressLint("CheckResult")
    fun getResult() {
        api.getResult("star", "au", "movie", null)
            .enqueue(object : Callback<ApiResult> {
                override fun onFailure(call: Call<ApiResult>?, er: Throwable?) {
                    Log.e("error", "" + er?.message)
                }

                override fun onResponse(call: Call<ApiResult>?, response: Response<ApiResult>?) {
                    realm.beginTransaction()
                    delete()
                    realm.insertOrUpdate(response?.body())
                    realm.commitTransaction()
                    view.onSuccessFetch()
                }

            })
    }

    fun getRealmResult(): List<Results> {
        return realm.copyFromRealm(realm.where(Results::class.java).findAll())
    }
    fun getRealmResult(name:String): List<Results> {
        return realm.copyFromRealm(realm.where(Results::class.java).contains("trackName",name,Case.INSENSITIVE).findAll())
    }
    fun delete(){
        realm.where(Results::class.java).findAll().deleteAllFromRealm()
    }
}