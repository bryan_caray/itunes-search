package com.sample.myapplication.activity.detail

import com.sample.myapplication.MvpPresenter

class DetailPresenter(view: DetailView) : MvpPresenter<DetailView>(view)