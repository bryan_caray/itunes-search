package com.sample.myapplication

import androidx.appcompat.app.AlertDialog
import io.realm.Realm

abstract class MvpView {

    interface ActivityFragment {

        val hasBusEvent: Boolean

        val layoutId: Int



        fun onViewCreated()
    }

    interface Presenter {

        val realm: Realm

        val api: ApiService
    }

    interface Dialog {

        val builder: AlertDialog.Builder

        var layoutId: Int

        fun onViewCreated()
    }
}