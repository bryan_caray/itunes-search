package com.yondu.basekotlin.global.mvp

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.FragmentTransaction
import com.sample.myapplication.*
import com.sample.myapplication.databinding.ToolbarBinding
import com.sample.myapplication.util.ActivityResultUtil
import com.sample.myapplication.util.ModalUtil
import com.yondu.canvas.global.util.PermissionUtil
import org.greenrobot.eventbus.EventBus

abstract class MvpActivity<B : ViewDataBinding, P : MvpPresenter<V>, V : MvpView.ActivityFragment> :
    AppCompatActivity(), MvpView.ActivityFragment, ActivityResultUtil.Handler, PermissionUtil.Handler,
    NetworkReceiver.OnNetworkChangeListener {

    protected val TAG = javaClass.simpleName

    abstract val presenter: P

    protected open lateinit var binding: B

    lateinit var permission: PermissionUtil

    lateinit var activityResult: ActivityResultUtil

    lateinit var modal: ModalUtil

    protected open val fragmentViewId: Int = 0

    protected open val toolbarBinding: ToolbarBinding? = null

    protected open var toolbarTitle = ""
    protected open var showBack = false

    protected open val toolbarTitleGravity = Gravity.START

    protected open val hasToolbarMenu = false

    protected open val toolbarMenu = 0

    protected open val toolbarIsDarkForeground = false

    protected open val hasToolbarExitEvent = false

    protected open val isInnerActivity = false

    protected open var isMainActivity = false

    protected open val isCloseActivity = false

    override val hasBusEvent get() = false

    private var backPressed = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BaseApplication.networkReceiver.setListener(this)

        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(BR.handler, this)

        permission = PermissionUtil(this, this)
        activityResult = ActivityResultUtil(this, this)
        modal = ModalUtil(binding, this)

        initToolbar()
        onViewCreated()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permission.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activityResult.onActivityResult(requestCode, resultCode, data)
    }

    private fun initToolbar() {
        if (toolbarIsDarkForeground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        toolbarBinding?.let { binding ->
            val toolbar = binding.toolbar
            setSupportActionBar(toolbar)

            toolbar.popupTheme = if (toolbarIsDarkForeground)
                R.style.ThemeOverlay_AppCompat_Dark
            else R.style.ThemeOverlay_AppCompat_Light

            toolbar.context.setTheme(
                if (toolbarIsDarkForeground)
                    R.style.ThemeOverlay_AppCompat_ActionBar
                else R.style.ThemeOverlay_AppCompat_Dark_ActionBar
            )

            if (isInnerActivity) supportActionBar?.let { actionBar ->
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setDisplayShowHomeEnabled(true)

                toolbar.navigationIcon = ContextCompat.getDrawable(
                    this,
                    if (!isCloseActivity) R.drawable.ic_left_arrow
                    else R.drawable.ic_left_arrow
                )

                toolbar.setNavigationOnClickListener(ToolbarListener())
            }

            supportActionBar?.let { actionBar ->
                actionBar.setDisplayShowTitleEnabled(false)
                val layoutParams =
                    Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
                layoutParams.gravity = toolbarTitleGravity

                binding.title.layoutParams = layoutParams
                binding.title.text = toolbarTitle
                binding.containerBack.visibility = if (showBack) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                binding.title.setTextAppearance(this, R.style.TextAppearance_AppCompat_Widget_ActionBar_Title)
            }
        }
    }

    override fun onBackPressed() {
        if (isMainActivity) {
            confirmExit()
        } else {
            super.onBackPressed()
        }

    }

    protected open fun onToolbarExitEvent() {
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return if (hasToolbarMenu) {
            menuInflater.inflate(toolbarMenu, menu)
            true
        } else {
            false
        }
    }

    fun goToActivity(activity: Class<out MvpActivity<*, *, *>>) {
        openActivity(activity)
        finish()
    }

    fun goToActivity(activity: Class<out MvpActivity<*, *, *>>, bundle: Bundle) {
        openActivity(activity, bundle)
        finish()
    }

    fun openActivity(activity: Class<out MvpActivity<*, *, *>>) {
        startActivity(Intent(this, activity))
    }

    fun openActivity(activity: Class<out MvpActivity<*, *, *>>, bundle: Bundle) {
        val intent = Intent(this, activity)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun openActivityTransition(
        activity: Class<out MvpActivity<*, *, *>>,
        bundle: Bundle,
        optionsCompat: ActivityOptionsCompat
    ) {
        val intent = Intent(this, activity)
        intent.putExtras(bundle)
        startActivity(intent, optionsCompat.toBundle())
    }

    fun openActivityForResult(activity: Class<out MvpActivity<*, *, *>>, requestCode: Int) {
        startActivityForResult(Intent(this, activity), requestCode)
    }

    fun openActivityForResult(activity: Class<out MvpActivity<*, *, *>>, requestCode: Int, bundle: Bundle) {
        val intent = Intent(this, activity)
        intent.putExtras(bundle)
        startActivityForResult(intent, requestCode)
    }

    protected fun openFragment(fragment: MvpFragment<*, *, *>, backStack: String) {
        supportFragmentManager.also { manager ->
            manager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).apply {

                manager.fragments.forEach { fragment ->
                    if (fragment.isVisible) hide(fragment)
                }

                manager.findFragmentByTag(backStack)?.let { fragment ->
                    show(fragment)
                } ?: run {
                    add(fragmentViewId, fragment, backStack)
                }

            }.commitNow()
        }

        toolbarBinding?.let { binding ->
            binding.title.text = fragment.toolbarTitle
        }
    }

    protected fun confirmExit() {
        val currentTime = System.currentTimeMillis()
        if (backPressed + 3000L > currentTime) super.onBackPressed()
        else modal.showToast("Press back again to exit")
        backPressed = currentTime
    }

    override fun onStart() {
        super.onStart()
        if (hasBusEvent) EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        if (hasBusEvent) EventBus.getDefault().unregister(this)
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(BaseApplication.networkReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(BaseApplication.networkReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }


    private inner class ToolbarListener : View.OnClickListener {
        override fun onClick(view: View) {
            supportFinishAfterTransition()
            if (!hasToolbarExitEvent) onBackPressed()
            else onToolbarExitEvent()
        }
    }
}