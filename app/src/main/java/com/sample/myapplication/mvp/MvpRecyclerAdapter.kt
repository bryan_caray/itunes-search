package com.sample.myapplication.mvp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class MvpRecyclerAdapter<B : ViewDataBinding, T>(private val layoutRes: Int, var items: List<T>) :
    RecyclerView.Adapter<MvpRecyclerAdapter<B, T>.ViewHolder>() {

    protected abstract fun onViewCreated(binding: B, item: T)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), layoutRes, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        onViewCreated(holder.binding, items[position])

    }

    inner class ViewHolder(val binding: B) : RecyclerView.ViewHolder(binding.root)
}