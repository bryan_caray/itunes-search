package com.sample.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

class NetworkReceiver : BroadcastReceiver() {

    private lateinit var listener: OnNetworkChangeListener

    override fun onReceive(context: Context, intent: Intent) {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        manager.activeNetworkInfo?.let {
            if (it.isConnected and it.isAvailable) listener.onNetworkConnected()
            else listener.onNetworkSlowConnection()
        } ?: run { listener.onNetworkDisconnected() }
    }

    fun setListener(listener: OnNetworkChangeListener) {
        this.listener = listener
    }

    interface OnNetworkChangeListener {
        fun onNetworkConnected() {}
        fun onNetworkDisconnected() {}
        fun onNetworkSlowConnection() {}
    }
}