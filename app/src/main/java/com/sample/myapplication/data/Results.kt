package com.sample.myapplication.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sample.myapplication.BaseModel
import io.realm.RealmModel
import io.realm.annotations.RealmClass

@RealmClass
open class Results : BaseModel, RealmModel {

    @SerializedName("wrapperType")
    @Expose
    var wrapperType: String = ""
    @SerializedName("kind")
    @Expose
    var kind: String = ""
    @SerializedName("collectionId")
    @Expose
    var collectionId: Int = 0
    @SerializedName("trackId")
    @Expose
    var trackId: Int = 0
    @SerializedName("artistName")
    @Expose
    var artistName: String = ""
    @SerializedName("collectionName")
    @Expose
    var collectionName: String = ""
    @SerializedName("trackName")
    @Expose
    var trackName: String = ""
    @SerializedName("collectionCensoredName")
    @Expose
    var collectionCensoredName: String = ""
    @SerializedName("trackCensoredName")
    @Expose
    var trackCensoredName: String = ""
    @SerializedName("collectionArtistId")
    @Expose
    var collectionArtistId: Int = 0
    @SerializedName("collectionArtistViewUrl")
    @Expose
    var collectionArtistViewUrl: String = ""
    @SerializedName("collectionViewUrl")
    @Expose
    var collectionViewUrl: String = ""
    @SerializedName("trackViewUrl")
    @Expose
    var trackViewUrl: String = ""
    @SerializedName("previewUrl")
    @Expose
    var previewUrl: String = ""
    @SerializedName("artworkUrl30")
    @Expose
    var artworkUrl30: String = ""
    @SerializedName("artworkUrl60")
    @Expose
    var artworkUrl60: String = ""
    @SerializedName("artworkUrl100")
    @Expose
    var artworkUrl100: String = ""
    @SerializedName("trackPrice")
    @Expose
    var trackPrice: Double = 0.0
    @SerializedName("collectionHdPrice")
    @Expose
    var collectionHdPrice: Double = 0.0
    @SerializedName("trackHdPrice")
    @Expose
    var trackHdPrice: Double = 0.0
    @SerializedName("releaseDate")
    @Expose
    var releaseDate: String = ""
    @SerializedName("collectionExplicitness")
    @Expose
    var collectionExplicitness: String = ""
    @SerializedName("trackExplicitness")
    @Expose
    var trackExplicitness: String = ""
    @SerializedName("discCount")
    @Expose
    var discCount: Int = 0
    @SerializedName("discNumber")
    @Expose
    var discNumber: Int = 0
    @SerializedName("trackCount")
    @Expose
    var trackCount: Int = 0
    @SerializedName("trackNumber")
    @Expose
    var trackNumber: Int = 0
    @SerializedName("trackTimeMillis")
    @Expose
    var trackTimeMillis: Int = 0
    @SerializedName("country")
    @Expose
    var country: String = ""
    @SerializedName("currency")
    @Expose
    var currency: String = ""
    @SerializedName("primaryGenreName")
    @Expose
    var primaryGenreName: String = ""
    @SerializedName("contentAdvisoryRating")
    @Expose
    var contentAdvisoryRating: String = ""
    @SerializedName("longDescription")
    @Expose
    var longDescription: String = ""
    @SerializedName("hasITunesExtras")
    @Expose
    var hasITunesExtras: Boolean = false
}