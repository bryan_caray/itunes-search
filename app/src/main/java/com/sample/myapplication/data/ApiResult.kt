package com.sample.myapplication.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sample.myapplication.BaseModel
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class ApiResult : RealmModel,BaseModel {

    @SerializedName("resultCount")
    @Expose
    open var resultCount: Int = 0


    @SerializedName("results")
    @Expose
    open lateinit var results: RealmList<Results>
}