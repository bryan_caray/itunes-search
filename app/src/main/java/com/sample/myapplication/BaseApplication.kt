package com.sample.myapplication

import android.util.Base64
import androidx.multidex.MultiDexApplication
import com.yondu.basekotlin.global.client.api.ApiClient
import io.realm.Realm
import io.realm.RealmConfiguration
import android.security.KeyPairGeneratorSpec
import java.security.*

class BaseApplication : MultiDexApplication() {

    companion object {

        @get:Synchronized
        val networkReceiver = NetworkReceiver()
    }

    override fun onCreate() {
        super.onCreate()
        setupRealmEncryption()
        ApiClient.createInstance()
    }

    private fun createRealmInstance(encryption: ByteArray) {
        val defaultVersion = 1L

        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder().run {
                name("${getString(R.string.app_name)}.realm")
                schemaVersion(defaultVersion)
                encryptionKey(encryption)
                if (BuildConfig.DEBUG) deleteRealmIfMigrationNeeded()
                else migration { realm, oldVersion, newVersion ->
                    val schema = realm.schema
                }
                build()
            })
    }

    private fun createRealmInstance() {
        val defaultVersion = 1L

        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder().run {
                name("${getString(R.string.app_name)}.realm")
                schemaVersion(defaultVersion)
                if (BuildConfig.DEBUG) deleteRealmIfMigrationNeeded()
                else migration { realm, oldVersion, newVersion ->
                    val schema = realm.schema
                }
                build()
            })
    }

    private fun setupRealmEncryption() {
        val keystore = KeyStore.getInstance("AndroidKeyStore").apply {
            load(null)
        }

        val aliases = keystore.aliases()

        var key: String? = null
        while (aliases.hasMoreElements()) {
            key = aliases.nextElement()
        }

        key?.let {
            val encryption = Base64.decode(it, Base64.NO_WRAP)
            createRealmInstance(encryption)
        } ?: run {
            val encryption = ByteArray(64)
            SecureRandom().nextBytes(encryption)
            val generatedKey = Base64.encodeToString(encryption, Base64.NO_WRAP)

            if (keystore.containsAlias(generatedKey)) {
                val generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore").apply {
                    val spec = KeyPairGeneratorSpec.Builder(applicationContext)
                        .setAlias(generatedKey)
                        .build()

                    initialize(spec)
                }

                generator.generateKeyPair()
                createRealmInstance(encryption)
            }else{
                createRealmInstance()
            }
        }
    }
}