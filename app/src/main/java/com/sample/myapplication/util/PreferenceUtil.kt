package com.yondu.canvas.global.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.sample.myapplication.Constant

class PreferenceUtil internal constructor(context: Context) {

    private var prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    companion object {

        private const val TOKEN = "token"
        private const val EMAIL = "email"
        private const val NAME = "name"
        private const val HASUSER = "hasUser"
        private const val HASWALKTHROUGH = "hasWalkthrough"
        @get:Synchronized
        lateinit var instance: PreferenceUtil

        fun createInstance(context: Context) {
            instance = PreferenceUtil(context)
        }
    }


    var hasWalkthrough
        set(name) = prefs.edit().putBoolean(PreferenceUtil.HASWALKTHROUGH, name).apply()
        get() = prefs.getBoolean(PreferenceUtil.HASUSER, false)!!

    var hasUser
        set(name) = prefs.edit().putBoolean(PreferenceUtil.HASUSER, name).apply()
        get() = prefs.getBoolean(PreferenceUtil.HASUSER, false)!!

    var name
        set(name) = prefs.edit().putString(PreferenceUtil.NAME, name).apply()
        get() = prefs.getString(PreferenceUtil.NAME, Constant.EMPTY_STRING)!!

    var email
        set(email) = prefs.edit().putString(PreferenceUtil.EMAIL, email).apply()
        get() = prefs.getString(PreferenceUtil.EMAIL, Constant.EMPTY_STRING)!!

    var token
        set(token) = prefs.edit().putString(PreferenceUtil.TOKEN, token).apply()
        get() = prefs.getString(PreferenceUtil.TOKEN, Constant.EMPTY_STRING)!!
}