package com.yondu.canvas.global.util

import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

object TextUtil {


    fun isEmail(email: CharSequence): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun setError(input: TextInputEditText, layout: TextInputLayout, error: CharSequence) {
        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable) {}

            override fun beforeTextChanged(source: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(source: CharSequence, start: Int, before: Int, count: Int) {
                layout.isErrorEnabled = false
                input.removeTextChangedListener(this)
            }
        })
        input.requestFocus()
        layout.error = error
    }
}