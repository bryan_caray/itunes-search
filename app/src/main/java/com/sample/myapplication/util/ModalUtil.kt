package com.sample.myapplication.util

import android.app.Activity
import android.content.Context
import android.widget.Toast
import androidx.databinding.ViewDataBinding

class ModalUtil(
    private val binding: ViewDataBinding,
    private val context: Context
) {
    fun showToast(s: String) {
        Toast.makeText(context,s,Toast.LENGTH_SHORT).show()
    }

}
