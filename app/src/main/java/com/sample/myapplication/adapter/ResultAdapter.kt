package com.sample.myapplication.adapter

import android.view.View
import com.sample.myapplication.R
import com.sample.myapplication.data.Results
import com.sample.myapplication.databinding.AdapterResultBinding
import com.sample.myapplication.mvp.MvpRecyclerAdapter

class ResultAdapter(results: List<Results>) :
    MvpRecyclerAdapter<AdapterResultBinding, Results>(R.layout.adapter_result, results) {

    lateinit var listener: Listener


    override fun onViewCreated(binding: AdapterResultBinding, item: Results) {
        if (item.artworkUrl100 != null) {
            binding.results = item
            binding.clItem.setOnClickListener(View.OnClickListener {
                onItemClick(binding,item)
            })
        }

    }

    fun onItemClick(
        binding: AdapterResultBinding,
        result: Results
    ) {
        listener.onDeliveryClick(result,binding)
    }

    interface Listener {
        fun onDeliveryClick(item: Results,binding: AdapterResultBinding)
    }
}