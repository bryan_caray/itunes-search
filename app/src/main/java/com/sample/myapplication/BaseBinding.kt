package com.sample.myapplication

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object BaseBinding {
    @JvmStatic
    @BindingAdapter("android:src")
    fun ImageView.setImageUrl(url: String?) {
        if (url != null) {
            Glide.with(context).load(url).placeholder(R.drawable.ic_video).into(this)
        }
    }
}