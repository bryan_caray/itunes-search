package com.yondu.basekotlin.global.client.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.sample.myapplication.ApiService
import com.sample.myapplication.BuildConfig
import com.sample.myapplication.Constant
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object {

        private const val TIMEOUT_DURATION = 60L

        @get:Synchronized
        lateinit var instance: ApiClient

        fun createInstance() {
            instance = ApiClient()
        }
    }

    val service: ApiService = retrofit.create(ApiService::class.java)

    val gson: Gson
        get() = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .setDateFormat(Constant.DATE_PARSE)
            .setPrettyPrinting()
            .create()

    private val retrofit
        get() = Retrofit.Builder()
            .baseUrl(BuildConfig.API_ROOT_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(safeClient)
            .build()

    private val okHttpBuilder
        get() = OkHttpClient.Builder()
            .readTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(
                LoggingInterceptor.Builder()
                    .loggable(true)
                    .request("apiClient_request")
                    .response("apiClient_response")
                    .log(Platform.INFO)
                    .setLevel(Level.BASIC)
                    .build()
            )

    private val safeClient get() = okHttpBuilder.build()
}